# Changelog

## [5.1.1] 2022-03-07

### Fixed
- Full auto with custom URL didn't deploy newly downloaded file (#13)

## [5.1.0] 2022-03-07

### Added
- Configurable timeout for deployment (ios-deploy --timeout ...)
- Configurable number of deployment retries before failing (was fixed to 4)

## [5.0.5] 2022-02-09

### Fixed
- Summary of failed deployments sorted by device name (#6)

## [5.0.4] 2022-02-09

### Changed
- Show summary of failed deployments after interactive deploy action (#6)

## [5.0.3] 2022-02-09

### Changed
- Show default IPA type as hint next to the device name when deploying (#5)

## [5.0.2] Initial public release
