import Select from 'enquirer/lib/prompts/select.js';
import InteractionCancelError from './InteractionCancelError.js';
import InteractionExitError from './InteractionExitError.js';

/**
 * @param {Object} options
 * @param {(string|Object)[]} options.choices
 * @param {boolean} options.cancel true if a "Cancel" option should be added
 * @param {boolean} options.exit true if an "Exit" option should be added
 */
function buildChoices({
  choices, cancel, exit,
}) {
  const result = [...choices];
  if (cancel && result.length) {
    result.push({ name: 'cancel', message: 'Cancel' });
  }
  if (exit && result.length) {
    result.push({ name: 'exit', message: 'Exit' });
  }
  return result;
}

export default class Menu extends Select {

  /**
   * @param {Object} options
   * @param {(string|Object)[]} options.choices
   * @param {boolean} options.cancel true if a "Cancel" option should be added
   * @param {boolean} options.exit true if an "Exit" option should be added
   */
  constructor({
    choices, cancel = false, exit = false, ...options
  }) {
    super({
      ...options,
      pointer: '>>> ',
      choices: buildChoices({
        choices, cancel, exit,
      }),
    });
  }

  /**
   * @returns {Promise<string>}
   * @throws {InteractionCancelError} if the user selected the 'Cancel' item
   * @throws {InteractionExitError} if the user selected the 'Exit' item
   */
  async run() {
    const result = await super.run();

    if (result === 'cancel') {
      throw new InteractionCancelError();
    }
    if (result === 'exit') {
      throw new InteractionExitError();
    }

    return result;
  }

}
