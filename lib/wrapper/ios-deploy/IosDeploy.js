import { spawn } from 'child_process';
import { join } from 'path';
import EventEmitter from 'events';
import { URL } from 'url';
import TimeoutError from './TimeoutError.js';

const dirname = new URL('.', import.meta.url).pathname;

const IOSDEPLOY_BINARY = join(dirname, '..', '..', '..', 'node_modules', '.bin', 'ios-deploy');

/**
 * @event IosDeploy#deviceFound
 * @type {Object}
 * @property {string} uuid
 * @property {string} name
 * @property {string} port
 */

/**
 * A NodeJS wrapper around ios-deploy
 */
export default class IosDeploy extends EventEmitter {

  /** @type {string} */
  #binary;

  /**
   * @param {Object} options
   * @param {string} options.iosdeployBinary
   */
  constructor({ iosdeployBinary = IOSDEPLOY_BINARY } = {}) {
    super();

    this.#binary = iosdeployBinary;
  }

  /**
   * Deploys an IPA file to the device with the given uuid
   * @param {Object} params
   * @param {string} params.uuid Device ID
   * @param {string} params.ipa Path to IPA file or unpacked IPA directory
   * @param {?number} params.timeout Timeout
   * @emits IosDeploy#progress
   * @returns {Promise<void>}
   * @throws {TimeoutError} Timeout during deployment
   * @throws {Error} Arbitrary error
   * @throws {number} ios-deploy exit code if != 0
   */
  install({ uuid, ipa, timeout = null }) {
    const params = [
      '--bundle', ipa,
      '--id', uuid,
    ];

    /** @param {string} output */
    const process = (output) => {
      if (output.match(/^\[\.\.\.\.\] Using /)) {
        this.emit('progress', { percentage: 0.0 });
      }
      const parsedOutput = output.match(/^\[\s*(\d+)%\] (?:Copying (.+) to device)?/);
      if (parsedOutput) {
        // eslint-disable-next-line no-unused-vars
        const [_, progressString, file] = parsedOutput;
        if (progressString) {
          const percentage = Number.parseInt(progressString, 10) / 100;
          this.emit('progress', { percentage, file });
        }
      }
    };

    return this.#spawnIosDeploy({ params, timeout, process });
  }

  /**
   * Removes a bundle from the device with the given uuid.
   * @param {Object} params
   * @param {string} params.uuid Device ID
   * @param {string} params.bundle AppId of the bundle to remove
   * @param {?number} params.timeout Timeout
   * @emits IosDeploy#progress
   * @returns {Promise<void>}
   * @throws {TimeoutError} Timeout during removal
   * @throws {Error} Arbitrary error
   * @throws {number} ios-deploy exit code if != 0
   */
  uninstall({ uuid, bundle, timeout = null }) {
    const params = [
      '--uninstall_only',
      '--bundle_id', bundle,
      '--id', uuid,
    ];

    /** @param {string} output */
    const process = (output) => {
      if (output.match(/^\[\.\.\.\.\] Using /)) {
        this.emit('progress', { percentage: 0.0 });
      }
      if (output.match(/^------ Uninstall phase ------/)) {
        this.emit('progress', { percentage: 0.2 });
      }
      if (output.match(/^\[ OK \] Uninstalled package with bundle id/)) {
        this.emit('progress', { percentage: 1.0 });
      }
    };

    return this.#spawnIosDeploy({ params, timeout, process });
  }

  /**
   * Scan for iDevices
   * @param {Object} params
   * @param {?number} params.timeout Timeout
   * @param {AbortSignal|undefined} params.abortSignal
   * @emits IosDeploy#deviceFound
   * @returns {Promise<Array<object>>}
   * @throws {Error} Arbitrary error
   * @throws {number} ios-deploy exit code if != 0
   */
  async scan({ timeout = null, abortSignal = undefined } = {}) {
    const params = ['--detect'];

    const options = { signal: abortSignal };

    /** @type {Object[]} */
    const result = [];

    /** @param {string} output */
    const process = (output) => {
      const parsedOutput = output.match(/^\[\.\.\.\.\] Found ([0-9a-fA-F-]+) .+?a\.k\.a\. '(.+?)' connected through (USB|WIFI)\./);
      if (parsedOutput) {
        // eslint-disable-next-line no-unused-vars
        const [_, uuid, name, port] = parsedOutput;
        const device = { uuid, name, port: port.toLowerCase() };
        this.emit('deviceFound', device);
        result.push(device);
      }
    };

    await this.#spawnIosDeploy({ params, timeout, options, process });

    return result;
  }

  /**
   * Execute ios-deploy with the given parameters, react properly to all execution interruptions,
   * and process the lines returned from stdout.
   * @param {Object} params
   * @param {string[]} params.params Command line parameters to ios-deploy
   * @param {?number} params.timeout Optional value for `ios-deploy --timeout` option
   * @param {Object} params.options Options for `spawn`
   * @param {function} params.process Callback to process stdout lines
   * @returns {Promise<void>}
   * @throws {TimeoutError} Timeout during deployment
   * @throws {Error} Arbitrary error
   * @throws {number} ios-deploy exit code if != 0
   */
  #spawnIosDeploy({
    params,
    timeout = null,
    options = {},
    process,
  }) {
    if (timeout) {
      params.push('--timeout', `${timeout}`);
    }

    return new Promise((resolve, reject) => {
      const child = spawn(this.#binary, Object.freeze(params), options);

      child.on('close', (code) => {
        if (code === 0) {
          resolve();
        } else {
          reject(code);
        }
      });

      child.on('error', (error) => {
        reject(error);
      });

      child.stdout.on('data', (buffer) => {
        buffer.toString()
          .trim()
          .split(/\n/)
          .forEach(process);
      });

      child.stderr.on('data', (buffer) => {
        const output = buffer.toString().trim();
        if (output.match(/Timed out waiting for device/)) {
          // Reject first, then kill.
          // This prevents the 'close'/'error' handler to call reject() before.
          reject(new TimeoutError());
          child.kill();
        } else {
          this.emit('error', { message: output });
        }
      });
    });
  }

}
