import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import IpaPacker from './IpaPacker.js';

use(chaiAsPromised);

describe('IpaPacker', () => {
  describe('#unpack()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const packer = new IpaPacker({ unzipBinary: './test/unzip-dummy.sh' });
      return expect(packer.unpack('abc.zip', 'def')).to.eventually.be.equal('def');
    });
  });
});
