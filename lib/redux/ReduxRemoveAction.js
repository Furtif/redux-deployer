import IpaDeployer from '../idevice/IpaDeployer.js';
import ProgressScreen from '../ui/ProgressScreen.js';

/**
 * Remove the given app from the device
 * @param {import('../models/Device').default} device
 * @param {string} appId
 * @param {import('../ui/ProgressBar').default} progressBar
 */
async function removeApp(device, appId, progressBar) {
  const { uuid } = device;

  const deployer = new IpaDeployer();

  try {
    await deployer.remove(uuid, appId);
    progressBar.done(`Remove complete: ${appId}`);
  } catch (/** @type {any} */ e) {
    progressBar.fail(`Remove failed: ${appId}`, e);
  }
}

/**
 * Removes the app from all devices in the given list
 * @param {Object} params
 * @param {import('./ReduxConfig').default} params.reduxConfig
 * @param {import('../models/DeviceList').default} params.deviceList
 * @param {?string} params.appId AppID to remove (Remove device's default IPA it not fiven)
 */
export default async function remove({ reduxConfig, deviceList, appId = null }) {
  const progress = new ProgressScreen(deviceList, { title: 'Remove app from devices' });
  await Promise.allSettled(
    deviceList.map(async (device) => {
      const deviceType = device.type;
      const ipaType = reduxConfig.ipaTypeList.get(deviceType);
      await removeApp(device, appId || ipaType?.appId, progress.bar({ item: device }));
    }),
  );
  progress.close();
}
