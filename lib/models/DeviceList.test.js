import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import DeviceList from './DeviceList.js';
import Device from './Device.js';

use(chaiAsPromised);

const TEXT_TEST = 'deviceFP:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot\n'
                  + '\n'
                  + '#commentedPhone:da84e3d67961dec2d72a5143a86bd4b702532d81:CHAIR\n'
                  + 'phonePanton:f52b79251dc31767cdfb233b1f9f9ab9c28ffa0e:CHAIR\n'
                  + 'deviceFP2:00008020-008D45480045BF78:flowerpot';
const JSON_TEST = JSON.stringify([
  {
    uuid: '49e0f931939bf3102418e36b785e5931021574de',
    name: 'phoneMoonlamp',
    type: 'MoonLamp',
  },
  {
    uuid: 'eed6657e924c869b79e0314f6dbf0255d62e3e5c',
    name: 'deviceFlowerpot',
    type: 'flowerpot',
  },
]);

describe('DeviceList', () => {
  describe('#loadFromString', () => {
    describe('called with plain text', async () => {
      /** @type {DeviceList} */
      let list;

      beforeEach(async () => {
        list = await DeviceList.loadFromString(TEXT_TEST);
      });

      it('should return a DeviceList (which is a Map)', () => {
        expect(list).to.be.instanceof(DeviceList);
        expect(list).to.be.instanceof(Map);
      });

      it('should skip empty lines and comments and return a Map with 3 properly keyed elements', () => {
        expect(list).to.have.lengthOf(3).and.have.all.keys('deviceFP', 'phonePanton', 'deviceFP2');
      });

      it('should return a Map with all elements of type Device', () => {
        expect(list.get('deviceFP')).to.be.instanceof(Device);
        expect(list.get('phonePanton')).to.be.instanceof(Device);
        expect(list.get('deviceFP2')).to.be.instanceof(Device);
      });

      it('should return a Map with properly parsed Device instances', async () => {
        expect(list.get('deviceFP').name).to.equal('deviceFP');
        expect(list.get('deviceFP').uuid).to.equal('eed6657e924c869b79e0314f6dbf0255d62e3e5c');
        expect(list.get('deviceFP').type).to.equal('flowerpot');
        expect(list.get('phonePanton').type).to.equal('CHAIR');
        expect(list.get('deviceFP2').type).to.equal('flowerpot');
        expect(list.get('deviceFP2').uuid).to.equal('00008020-008D45480045BF78');
      });
    });

    describe('called with json', async () => {
      /** @type {DeviceList} */
      let list;

      beforeEach(async () => {
        list = await DeviceList.loadFromString(JSON_TEST);
      });

      it('should return a DeviceList (which is a Map)', () => {
        expect(list).to.be.instanceof(DeviceList);
        expect(list).to.be.instanceof(Map);
      });

      it('should return a Map with 2 properly keyed elements', () => {
        expect(list).to.have.lengthOf(2).and.have.all.keys('phoneMoonlamp', 'deviceFlowerpot');
      });

      it('should return a Map with all elements of type Device', () => {
        expect(list.get('phoneMoonlamp')).to.be.instanceof(Device);
        expect(list.get('deviceFlowerpot')).to.be.instanceof(Device);
      });

      it('should return a Map with properly parsed Device instances', async () => {
        expect(list.get('phoneMoonlamp').name).to.equal('phoneMoonlamp');
        expect(list.get('phoneMoonlamp').uuid).to.equal('49e0f931939bf3102418e36b785e5931021574de');
        expect(list.get('phoneMoonlamp').type).to.equal('MoonLamp');
        expect(list.get('deviceFlowerpot').type).to.equal('flowerpot');
      });
    });

    describe('called with invalid plain text', async () => {
      it('should fail', async () => {
        const testText = 'eed6657e924c869b79e0314f6dbf0255d62e3e5c:deviceFP:flowerpot';
        return expect(DeviceList.loadFromString(testText)).to.be.rejectedWith('Error parsing device list');
      });
    });
  });

  describe('#filter', () => {
    /** @type {DeviceList} */
    let list;

    beforeEach(async () => {
      list = await DeviceList.loadFromString(TEXT_TEST);
    });

    it('should return the same list when called without filter', async () => {
      const filteredList = list.filter(null);
      expect(filteredList).to.be.instanceof(DeviceList);
      expect(filteredList).to.be.equal(list);
    });

    it('should return a filtered list when called with array of keys', async () => {
      const filteredList = list.filter(['deviceFP2', 'phonePanton']);
      expect(filteredList).to.be.instanceof(DeviceList);
      expect(filteredList).to.have.lengthOf(2).and.have.all.keys('phonePanton', 'deviceFP2');
    });

    it('should return a filtered list when called with a function', async () => {
      const filteredList = list.filter(([_, device]) => device.uuid.match(/^eed/));
      expect(filteredList).to.be.instanceof(DeviceList);
      expect(filteredList).to.have.lengthOf(1).and.have.all.keys('deviceFP');
    });

    it('should throw if called with invalid filter', async () => {
      // @ts-ignore
      expect(() => list.filter('no strings attached')).to.throw();
    });
  });

  describe('#map', () => {
    /** @type {DeviceList} */
    let list;

    beforeEach(async () => {
      list = await DeviceList.loadFromString(TEXT_TEST);
    });

    it('should return an array with mapped elements', () => {
      const map = list.map((device) => !!device.name.match(/^phone/));
      expect(map).to.be.an('array').with.lengthOf(3);
      expect(map[0]).to.be.a('boolean').equals(false);
      expect(map[1]).to.be.a('boolean').equals(true);
      expect(map[2]).to.be.a('boolean').equals(false);
    });

    it('should return an array with the list elements when called without function', () => {
      const map = list.map();
      expect(map).to.be.an('array').with.lengthOf(3);
      expect(map[0]).to.be.equal(list.get('deviceFP'));
      expect(map[1]).to.be.equal(list.get('phonePanton'));
      expect(map[2]).to.be.equal(list.get('deviceFP2'));
    });
  });

  describe('#getTypes', () => {
    it('should return properly counted device types', async () => {
      const list = await DeviceList.loadFromString(TEXT_TEST);
      const types = list.getTypes();
      expect(types).to.be.an('object');
      expect(types).to.have.all.keys('flowerpot', 'CHAIR');
      expect(types.flowerpot).to.equal(2);
      expect(types.CHAIR).to.equal(1);
    });
  });

  describe('#getIpaTypeIds', () => {
    it('should return an array with type ids', async () => {
      const list = await DeviceList.loadFromString(TEXT_TEST);
      const typeIds = list.getIpaTypeIds();
      expect(typeIds).to.be.an('array').with.lengthOf(2);
      expect(typeIds).to.contain('flowerpot');
      expect(typeIds).to.contain('CHAIR');
    });
  });

  describe('#getPorts', () => {
    it('should return properly counted device ports', async () => {
      const list = await DeviceList.loadFromString(TEXT_TEST);
      list.get('deviceFP2').port = 'usb';

      const ports = list.getPorts();
      expect(ports).to.be.an('object');
      expect(ports).to.have.all.keys('null', 'usb');
      expect(ports.null).to.equal(2);
      expect(ports.usb).to.equal(1);
    });
  });

});
