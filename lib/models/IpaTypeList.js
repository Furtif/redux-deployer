import IpaType from './IpaType.js';

export default class IpaTypeList extends Map {

  /**
   * Returns a populated IPA Type list from the given config data
   * @param {Object[]} configs
   * @returns {Promise<IpaTypeList>}
   * @throws
   */
  static async loadFromConfig(configs) {
    if (!Array.isArray(configs)) throw new Error('Config not an array');
    const ipaTypes = configs.map(({ id, ...config }) => new IpaType(id, config));
    return new IpaTypeList(ipaTypes.map((ipaType) => [ipaType.id, ipaType]));
  }

  /**
   * Returns a new IpaTypeList filtered by the given IPA type ids
   * @param {(function([string,any]):any)|string[]|null} filter Filter (either function or list of
   *                                                            IPA type ids, default: no filter)
   * @returns {IpaTypeList}
   */
  filter(filter = null) {
    if (filter === null) return this;

    let filterFn;
    if (Array.isArray(filter)) {
      filterFn = ([key, _]) => filter.indexOf(key) >= 0;
    } else if (typeof filter === 'function') {
      filterFn = filter;
    } else {
      throw new Error(`Unknown filter: ${filter}`);
    }

    return new IpaTypeList([...this].filter(filterFn));
  }

  /**
   * Applies the given callback function to the IPA list and returns an array
   * @param {function(IpaType, string): any} fn
   * @returns {Array<any>}
   */
  map(fn = (ipaType) => ipaType) {
    return [...this].map(([key, ipaType]) => fn(ipaType, key));
  }

}
