import EventEmitter from 'events';

export default class Device extends EventEmitter {

  static PORT_USB = 'usb';

  static PORT_WIFI = 'wifi';

  /** @type {string} */
  #uuid;

  /** @type {string} */
  #name;

  /** @type {?string} */
  #port;

  /** @type {?string} */
  #type;

  /**
   * Creates a new device
   * @param {string} uuid Device UUID
   * @param {Object} params
   * @param {?string} params.name Device name (defaults to UUID)
   * @param {?string} params.port Connector port (wifi|usb)
   * @param {?string} params.type IPA typeId used on this device by default
   */
  constructor(uuid, { name = null, port = null, type = null } = {}) {
    super();
    this.#uuid = uuid;
    this.#name = name || uuid;
    this.#port = port;
    this.#type = type;
  }

  /**
   * @returns {string}
   */
  get uuid() {
    return this.#uuid;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.#name;
  }

  /**
   * @returns {string|null}
   */
  get port() {
    return this.#port;
  }

  /**
   * @param {string|null} port
   */
  set port(port) {
    if (port === null || ![Device.PORT_USB, Device.PORT_WIFI].includes(port)) {
      throw new Error(`Unknown port for device ${this.#name}: ${port}`);
    }
    this.#port = port;
    this.emit('update', this);
  }

  /**
   * @returns {string|null}
   */
  get type() {
    return this.#type;
  }

}
