import { expect } from 'chai';
import HttpDownloader from './HttpDownloader.js';
import MegaDownloader from './MegaDownloader.js';
import { getDownloaderClassFor, createDownloaderForUrl } from './Factory.js';

const MEGANZ_TEST_URL = 'https://mega.nz/file/XRICCZzA#-qU7sPYlYywWIaH0MN035okyegUKwDMescMHufQ7IpU';

describe('downloader/Factory', () => {
  describe('getDownloaderClassFor', () => {
    it('should return the default class for any domain', () => {
      const clazz = getDownloaderClassFor('example.com');
      expect(clazz).to.be.equal(HttpDownloader);
    });

    it('should return a special class for mega.nz', () => {
      const clazz = getDownloaderClassFor('mega.nz');
      expect(clazz).to.be.equal(MegaDownloader);
    });
  });

  describe('createDownloaderForUrl', () => {
    it('should return an instance of default downloader', () => {
      const downloader = createDownloaderForUrl('https://example.com');
      expect(downloader).to.be.instanceOf(HttpDownloader);
    });

    it('should return an instance of a mega.nz downloader', () => {
      const downloader = createDownloaderForUrl(MEGANZ_TEST_URL);
      expect(downloader).to.be.instanceOf(MegaDownloader);
    });
  });
});
