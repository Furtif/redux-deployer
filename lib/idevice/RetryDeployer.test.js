/* eslint-disable max-classes-per-file */
import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import EventEmitter from 'events';
import RetryDeployer from './RetryDeployer.js';

use(chaiAsPromised);

// Ducktyping a test dummy Deployer
class DummyDeployer extends EventEmitter {

  constructor(attemptsNeeded = 1) {
    super();
    this.attemptsNeeded = attemptsNeeded;
  }

  deploy(uuid, ipa) {
    return new Promise((res, rej) => {
      --this.attemptsNeeded ? rej(new Error(this.attemptsNeeded)) : res({ uuid, ipa });
    });
  }

}

class ProgressMeterDummy {

  constructor() {
    /** @type {number[]} */
    this.result = [];
  }

  set({ /** @type {number} */ percentage }) {
    this.result.push(percentage);
  }

}

describe('RetryDeployer', () => {
  describe('#deploy()', () => {
    it('should succeed without retries', () => {
      const dummy = new DummyDeployer();
      const deployer = new RetryDeployer(dummy, { reries: 3 });
      return (
        expect(deployer.deploy('abc', 'def.ipa')).to.eventually.be.an('object').which.include.keys('uuid', 'ipa')
          .then(() => expect(deployer.attempt).to.be.equal(1))
      );
    });

    it('should succeed after 3 attempts if the 3rd is successful', () => {
      const dummy = new DummyDeployer(3);
      const deployer = new RetryDeployer(dummy, { reries: 3 });
      const deployPromise = deployer.deploy('abc', 'def.ipa');
      return (
        expect(deployPromise).to.eventually.be.an('object').which.include.keys('uuid', 'ipa')
          .then(() => expect(deployer.attempt).to.be.equal(3))
      );
    });

    it('should fail after 3 attempts if te 3rd also fails', () => {
      const dummy = new DummyDeployer(10);
      const deployer = new RetryDeployer(dummy, { reries: 3 });
      const deployPromise = deployer.deploy('abc', 'def.ipa');
      return (
        expect(deployPromise).to.eventually.be.rejected
          .then(() => expect(deployer.attempt).to.be.equal(3))
      );
    });

    it('should succeed after 3 attempts and execute callback 2 times', () => {
      const dummy = new DummyDeployer(3);
      let retryCallbackCalled = 0;
      const deployer = new RetryDeployer(dummy, {
        retries: 3,
        retryCallback: () => retryCallbackCalled++,
      });
      const deployPromise = deployer.deploy('abc', 'def.ipa');
      return (
        expect(deployPromise).to.eventually.be.an('object').which.include.keys('uuid', 'ipa')
          .then(() => expect(retryCallbackCalled).to.be.equal(2))
      );
    });

  });
});
