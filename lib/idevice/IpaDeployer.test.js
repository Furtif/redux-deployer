import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import IpaDeployer from './IpaDeployer.js';
import ProgressMeter from '../util/ProgressMeter.js';
import IosDeploy from '../wrapper/ios-deploy/IosDeploy.js';

use(chaiAsPromised);
use(sinonChai);

describe('IpaDeployer', () => {

  /** @type {sinon.SinonSpy|typeof IosDeploy} */
  let IosDeployClassStub;
  /** @type {IosDeploy} */
  let iosDeployStub;
  /** @type {?function} */
  let progressmeterSet = null;
  /** @type {?function} */
  let progressmeterError = null;

  beforeEach(() => {
    iosDeployStub = new IosDeploy();
    sinon.stub(iosDeployStub, 'install');
    sinon.stub(iosDeployStub, 'uninstall');
    IosDeployClassStub = sinon.spy(() => iosDeployStub);
    progressmeterSet = sinon.stub(ProgressMeter.prototype, 'set');
    progressmeterError = sinon.stub(ProgressMeter.prototype, 'error');
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('#deploy()', () => {
    it('should eventually succeed', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });

      const result = await deployer.deploy('abc', 'def.ipa');

      expect(iosDeployStub.install).to.be.calledOnce;
      expect(result).to.be.deep.equal({ uuid: 'abc', ipa: 'def.ipa' });
    });

    it('should reject if ios-deply throws', () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.install.throws();

      const deployPromise = deployer.deploy('abc', 'def.ipa');

      return expect(deployPromise).to.eventually.rejectedWith(Error);
    });

    it('should reject if ios-deply throws exit code', () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.install.throws(1);

      const deployPromise = deployer.deploy('abc', 'def.ipa');

      return expect(deployPromise).to.eventually.rejectedWith('ios-deploy exited with code 1');
    });

    it('should relay ios-deploy progress events to ProgressMeter', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.install.callsFake(() => {
        iosDeployStub.emit('progress', { percentage: 0.0 });
        iosDeployStub.emit('progress', { percentage: 0.5, file: 'ghi' });
      });

      await deployer.deploy('abc', 'def.ipa');

      expect(progressmeterSet).to.be.calledTwice;
      expect(progressmeterError).to.not.be.called;
      expect(progressmeterSet.getCall(0)).to.be.calledWithMatch({ percentage: 0.0, message: 'Deploy' });
      expect(progressmeterSet.getCall(1)).to.be.calledWithMatch({ percentage: 0.5, message: 'Deploy (ghi)' });
    });

    it('should relay ios-deploy error events to ProgressMeter', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.install.callsFake(() => {
        iosDeployStub.emit('error', { message: 'ghi' });
      });

      await deployer.deploy('abc', 'def.ipa');

      expect(progressmeterError).to.be.calledOnce;
      expect(progressmeterError).to.be.calledWith('ghi');
    });
  });

  describe('#remove()', () => {
    it('should eventually succeed', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });

      const result = await deployer.remove('abc', 'def');

      expect(iosDeployStub.uninstall).to.be.calledOnce;
      expect(result).to.be.deep.equal({ uuid: 'abc', bundle: 'def' });
    });

    it('should reject if ios-deply throws', () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.uninstall.throws();

      const deployPromise = deployer.remove('abc', 'def');

      return expect(deployPromise).to.eventually.rejectedWith(Error);
    });

    it('should reject if ios-deply throws exit code', () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.uninstall.throws(1);

      const deployPromise = deployer.remove('abc', 'def');

      return expect(deployPromise).to.eventually.rejectedWith('ios-deploy exited with code 1');
    });

    it('should relay ios-deploy progress events to ProgressMeter', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.uninstall.callsFake(() => {
        iosDeployStub.emit('progress', { percentage: 0.0 });
        iosDeployStub.emit('progress', { percentage: 0.5, file: 'ghi' });
      });

      await deployer.remove('abc', 'def');

      expect(progressmeterSet).to.be.calledTwice;
      expect(progressmeterError).to.not.be.called;
      expect(progressmeterSet.getCall(0)).to.be.calledWithMatch({ percentage: 0.0 });
      expect(progressmeterSet.getCall(1)).to.be.calledWithMatch({ percentage: 0.5 });
    });

    it('should relay ios-deploy error events to ProgressMeter', async () => {
      const deployer = new IpaDeployer({ iosdeployClass: IosDeployClassStub });
      iosDeployStub.uninstall.callsFake(() => {
        iosDeployStub.emit('error', { message: 'ghi' });
      });

      await deployer.remove('abc', 'def');

      expect(progressmeterError).to.be.calledOnce;
      expect(progressmeterError).to.be.calledWith('ghi');
    });
  });
});
