import { basename } from 'path';
import EventEmitter from 'events';
import ProgressMeter from '../util/ProgressMeter.js';
import IosDeploy from '../wrapper/ios-deploy/IosDeploy.js';

/**
 * Deploys an IPA to a device using ios-deploy
 */
export default class IpaDeployer extends EventEmitter {

  /** @type {typeof IosDeploy} */
  #iosdeployClass;

  /**
   * @param {Object} options
   * @param {typeof IosDeploy} options.iosdeployClass
   */
  constructor({ iosdeployClass = IosDeploy } = {}) {
    super();

    this.#iosdeployClass = iosdeployClass;
  }

  /**
   * Deploys an IPA file to the device with the given uuid
   * @param {string} uuid Device ID
   * @param {string} ipa Path to IPA file or unpacked IPA directory
   * @param {number} timeout
   * @param {ProgressMeter} meter
   * @returns {Promise<object>} object = { uuid, ipa }
   */
  async deploy(uuid, ipa, timeout = 30, meter = new ProgressMeter()) {
    const iosdeploy = new this.#iosdeployClass();

    iosdeploy.on('progress', ({ percentage, file }) => {
      const fileInfo = file ? ` (${basename(file)})` : '';
      meter.set({ percentage, message: `Deploy${fileInfo}`, file });
    });
    iosdeploy.on('error', ({ message }) => meter.error(message));

    try {
      await iosdeploy.install({ uuid, ipa, timeout });
    } catch (e) {
      if (typeof e === 'number') {
        throw new Error(`ios-deploy exited with code ${e}`);
      }
      throw e;
    }

    return { uuid, ipa };
  }

  /**
   * Removes a IPA from the device with the given uuid.
   * @param {string} uuid Device ID
   * @param {string} bundle AppId of the bundle to remove
   * @param {number} timeout
   * @param {ProgressMeter} meter
   * @returns {Promise<object>} object = { uuid, bundle }
   */
  async remove(uuid, bundle, timeout = 30, meter = new ProgressMeter()) {
    const iosdeploy = new this.#iosdeployClass();

    iosdeploy.on('progress', ({ percentage }) => meter.set({ percentage }));
    iosdeploy.on('error', ({ message }) => meter.error(message));

    try {
      await iosdeploy.uninstall({ uuid, bundle, timeout });
    } catch (e) {
      if (typeof e === 'number') {
        throw new Error(`ios-deploy exited with code ${e}`);
      }
      throw e;
    }

    return { uuid, bundle };
  }

}
