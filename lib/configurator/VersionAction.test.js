import { expect } from 'chai';
import VersionAction from './VersionAction.js';

describe('configurator/VersionAction', () => {
  describe('#toString', () => {
    it('should return a nice string', () => {
      const config = { version: '1.01' };
      const action = new VersionAction(config);
      expect(`${action}`).to.equal('Set version to 1.01');
    });
  });
});
