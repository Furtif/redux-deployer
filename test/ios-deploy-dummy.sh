#! /bin/sh --posix

echo "[....] Waiting up to 60 seconds for iOS device to be connected"

if [ x$1 == x--detect ]; then
  for i in $(seq 1 10); do
    phone=iPhone$RANDOM
    hash=$(echo $phone | shasum | cut -f1 -d ' ')
    if [ $(( i % 3 )) -eq 1 ]; then
      port=WIFI
    else
      port=USB
    fi
    echo "[....] Found $hash (N66AP, iPhone 6s Plus, iphoneos, arm64, 14.4.2, ) a.k.a. '$phone' connected through $port."
  done
else
  for i in $(seq 1 10); do
    phone=iPhone$RANDOM
    hash=$(echo $phone | shasum | cut -f1 -d ' ')
    echo "Skipping $hash (N66AP, iPhone 6s Plus, iphoneos, arm64, 14.4.2, ) a.k.a. '$phone'."
  done

  phone=iPhone$RANDOM
  hash=$(echo $phone | shasum | cut -f1 -d ' ')
  echo "[....] Using $hash (N66AP, iPhone 6s Plus, iphoneos, arm64, 14.4.2, ) a.k.a. '$phone'."

  if [ x$1 == x--uninstall_only ]; then
    echo "------ Uninstall phase ------"
    echo "[ OK ] Uninstalled package with bundle id flowerpot"
  else
    echo "------ Install phase ------"
    echo "[  0%] Found $hash (N66AP, iPhone 6s Plus, iphoneos, arm64, 14.4.2, ) a.k.a. '$phone' connected through WIFI, beginning install"
    for i in $(seq 1 100); do
      echo "[$(printf %3s $i)%] Copying $RANDOM/$RANDOM.plist to device"
      # sleep $(awk -v min=0 -v max=1 'BEGIN { srand(); print min+rand()*(max-min) }')
    done
  fi
fi
